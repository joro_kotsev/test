from seleniumbase.common import encryption

if __name__ == '__main__':
    print('Enter password: ', end='')
    plain_password = input()
    encrypted_password = encryption.decrypt(plain_password)
    print('Encrypted password is: ', encrypted_password)
