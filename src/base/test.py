import allure
from parameterized import parameterized

from src.base.base import BaseTools
from src.globals.addresses import ApiGlobals


class Test():

    request_valid_transaction = {
                                    "payment_transaction": {
                                        "card_number": "4200000000000000",
                                        "cvv": "123",
                                        "expiration_date": "06/2019",
                                        "amount": "500",
                                        "usage": "Coffeemaker",
                                        "transaction_type": "sale",
                                        "card_holder": "Panda Panda",
                                        "email": "panda@example.com",
                                        "address": "Panda Street, China"
                                    }
                                }

    invalid_void_response = {
                                "reference_id": [
                                    "Invalid reference transaction!"
                                ]
                            }

    @allure.story('Valid payment transaction')
    def test_valid_payment_transaction(self):
        # Generate valid payment request
        request_body = BaseTools.generate_valid_payment_request_body(self,
                                                                     "4200000000000000",
                                                                     "123",
                                                                     "06/2019",
                                                                     "500",
                                                                     "Coffeemaker",
                                                                     "sale",
                                                                     "Panda Panda",
                                                                     "panda@example.com",
                                                                     "Panda Street, China")

        # Post payment and verify success status code
        response =  BaseTools.post_payment_transaction(self, ApiGlobals.username, ApiGlobals.password, request_body)
        assert response.status_code == 200
        response_json = response.json()

        # Verify response
        BaseTools.verify_valid_response(self, response_json,
                                        "approved",
                                        "Coffeemaker",
                                        500,
                                        "Your transaction has been approved." )


    @allure.story('Invalid payment transaction')
    def test_invalid_payment_transaction(self):
        request_body = Test.request_valid_transaction

        combinations_dict = [(ApiGlobals.username, "wrongpass"),
                             ("wrong_user", ApiGlobals.password),
                             ("wrong_user", "wrong_pass")]

        for (username, password) in combinations_dict:
            response = BaseTools.post_payment_transaction(self, username, password, request_body)
            assert response.status_code == 401


    @allure.story('Void transaction success')
    def test_void_transaction_success(self):
        # Generate valid ref id
        request_valid_transaction = Test.request_valid_transaction
        reference_id = BaseTools.get_valid_reference_id(self, ApiGlobals.username, ApiGlobals.password, request_valid_transaction)

        # Post void transaction and assert status code
        request_body_void_transaction = {
                                            "payment_transaction": {
                                                "reference_id": reference_id,
                                                "transaction_type": "void"
                                            }
                                        }

        response_void_transaction = BaseTools.post_payment_transaction(self, ApiGlobals.username, ApiGlobals.password,
                                                                       request_body_void_transaction)
        assert response_void_transaction.status_code == 200
        response_void_transaction_json = response_void_transaction.json()

        # Verify response
        BaseTools.verify_valid_response(self, response_void_transaction_json,
                                        "approved",
                                        "Coffeemaker",
                                        500,
                                        "Your transaction has been voided successfully")



    @parameterized.expand([('7e59f0116b74d62e3980ee0231b02dc4'),
                           ('2f256d59ce3472459704dc43040f0788'),
                           ('0e08644635ccb520c2eeb54f33865660')
                           ])
    @allure.story('Void transaction exsistent payment - Invalid reference')
    def test_void_transaction_invalid_reference(self, reference_id):
        request_body_void_transaction = BaseTools.generate_void_transaction_request_body(self, reference_id)

        response_void_transaction = BaseTools.post_payment_transaction(self, ApiGlobals.username, ApiGlobals.password, request_body_void_transaction)
        assert response_void_transaction.status_code == 422
        response_void_transaction_json = response_void_transaction.json()

        response_to_verify = {
                                "reference_id": [
                                    "Invalid reference transaction!"
                                ]
                            }

        assert (response_void_transaction_json) == (response_to_verify)


    @allure.story('Void transaction non-existent payment - Invalid reference')
    def test_void_transaction_non_existent(self):
        request_body_void_transaction = BaseTools.generate_void_transaction_request_body(self, "nonexistentid")

        response_void_transaction = BaseTools.post_payment_transaction(self, ApiGlobals.username, ApiGlobals.password,
                                                                       request_body_void_transaction)
        assert response_void_transaction.status_code == 422
        response_void_transaction_json = response_void_transaction.json()

        assert (response_void_transaction_json) == (Test.invalid_void_response)