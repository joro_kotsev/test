## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. 

### Prerequisites

What things you need to install the software and how to install them:
- PyCharm - Python IDE
- Python - programming language 
- pipenv - tool that provides all necessary to create a virtual environment for your Python project
- Allure - test report tool
- pytest - helps to write small tests better and support complex functional testing for applications and libraries
- requests - the requests library is the standard for making HTTP requests in Python
- seleniumBase - Python framework for Web UI testing and site tours with Selenium and pytest

### Installing

A step by step series of examples that tell you how to get a development env running
- Install Python IDE - for example Pycharm
- Install Python (last stable version 3.8) - contains standard Python libraries
- Install pipenv
```
Install pipenv:
pip install pipenv
```
- Start a new Python project. 
- Set the project interpreter:
    - Go to project interpreter ```File->Setting -> Project:{project_name} -> Project Interpreter```
    - Click on wheel button to create a new environment
    - Choose “Add...” to create a new project interpreter 
    - From “Add Python Interpreter” dialog window select Pipenv Environment
    - Set the base Python interpreter
- After installing the pip environment, open the Pipfile to create more libraries. In [package] add the list with libraries.
```
[packages]
pytest = "*"
selenium = "*"
seleniumbase = "*"
allure-pytest = "*"
pillow = "*"
requests = "*"
pyjwt = "*"
```
Also is necessary to fix the Python version in Pipfile.
```
[requires]
python_version = "3.8"
```
The described packages will be installed as a part of pipenv libraries. The process automatically  generated the file Pipfile.lock with all dependencies. 

## Set Testing environment
- Install Oracle VM Virtual Box in order to set up Linux virtual machine on your PC
- Follow up "Ubuntu set up" on Readme file "https://github.com/eMerchantPay/codemonsters_api_full"
- Configuration username and pass are set in globals.addresses.py file

Install Postman on your PC
Run queries 

## Running the tests

- Set the test execution configuration in PyCharm - ```Run -> Edit Configuration``` in *Additional Arguments* field
```
--alluredir ..\..\tools\allure-report\
```
- Start the test execution from PyCharm
- Run the Allure report in PyCharm after the execution (allure_show.reports.cmd) to generate a new report

## Obfuscate password
- Run the obfuscate_password.cmd
- enter your pass, e.g. 'codemonster'
- press Enter

## Authors

* **Georgi Kotsev Automation QA 
