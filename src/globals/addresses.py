from seleniumbase.common import encryption

class ApiGlobals(object):
    main_ip = '192.168.56.101'

    payment_transactions_address = 'http://' + f'{main_ip}:3001/payment_transactions'

    username = 'codemonster'

    password = encryption.decrypt('$^*ENCRYPT=Vwl0A0tSc1YbT0IkJy4QMBpv?&#$')

    #password =  'my5ecret-key2o2o'
