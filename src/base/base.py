from datetime import datetime

import allure
import requests
from requests.auth import HTTPBasicAuth


class BaseTools():

    @allure.step('Post Payment Transaction')
    def post_payment_transaction(self, username, passs, data):
        from src.globals.addresses import ApiGlobals
        res = requests.post(ApiGlobals.payment_transactions_address,
                            json=data,
                            auth=HTTPBasicAuth(username, passs),
                            headers={
                                'Content-type': 'application/json',
                            }
                       )
        return res

        allure.attach(res, name='result_data', attachment_type=allure.attachment_type.JSON)


    @allure.step('Get valid reference id')
    def get_valid_reference_id(self, username, passs, data):
        response = BaseTools.post_payment_transaction(self, username, passs, data)
        response_json = response.json()
        unique_id = response_json['unique_id']
        return unique_id


    @allure.step('Verify valid responce')
    def verify_valid_response(self, json_data, status, usage, amount, message ):
        assert len(json_data["unique_id"]) == 32
        assert json_data["status"] == status
        assert json_data["usage"] == usage
        assert json_data["amount"] == amount
        assert json_data["usage"] == usage
        assert type(json_data["transaction_time"]) == str
        assert json_data["message"] == message

        # transaction_type = json_data["transaction_time"]
        # now = datetime.utcnow().isoformat()
        # assert now <= transaction_type


    @allure.step('Generate valid payment transaction request body')
    def generate_valid_payment_request_body(self, card_number, cvv, expiration_date, amount, usage,
                                            transaction_type, card_holder, email, address):
        request_body =  {
                            "payment_transaction": {
                                "card_number": card_number,
                                "cvv": cvv,
                                "expiration_date": expiration_date,
                                "amount": amount,
                                "usage": usage,
                                "transaction_type": transaction_type,
                                "card_holder": card_holder,
                                "email": email,
                                "address": address
                            }
                        }

        return request_body


    @allure.step('Generate void transaction request body')
    def generate_void_transaction_request_body(self, reference_id):

        request_body_void_transaction = {
                                            "payment_transaction": {
                                                "reference_id": reference_id,
                                                "transaction_type": "void"
                                            }
                                        }
        return request_body_void_transaction
